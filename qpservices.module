<?php

/**
 * The main file for qpservices.
 * @file
 */

/**
 * Implementation of hook_menu().
 */
function qpservices_menu() {
  $items = array();

  $items['admin/settings/qpservices'] = array(
    'title' => t('QP Services'),
    'description' => t('Settings and options for the QP Services module.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('qpservices_admin_form'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer qpservices'),
    'file' => 'qpservices.admin.inc'
  );
  
  $items['svg/amplify/%/flamboyance.svg'] = array(
    'title' => t('Flamboyance'),
    'page callback' => 'qpservices_amplify_flamboyance_svg',
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  
  $items['svg/amplify/%/education.svg'] = array(
    'title' => t('Education'),
    'page callback' => 'qpservices_amplify_education_svg',
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  
  $items['svg/amplify/%/slang.svg'] = array(
    'title' => t('Slang'),
    'page callback' => 'qpservices_amplify_slang_svg',
    'page arguments' => array(2),
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function qpservices_perm() {
  return array(
    'administer qpservices',
  );
}


/**
 * Implementation of hook_help().
 */
function qpservices_help($path, $args) {
  if ($path == 'admin/help#qpservices') {
    return t('This module contains several sample QueryPath tools.');
  }
}

/**
 * Implementation of hook_block().
 */
function qpservices_block($op = 'list', $delta = 0, $edit = NULL) {
  if ($op == 'list') {
    $blocks['twitter'] = array(
      'info' => t('Twitter Search for QueryPath'),
    );
    $blocks['flickr'] = array(
      'info' => t('Flickr 10'),
    );
    $blocks['flickrset'] = array(
      'info' => t('Flickr Photoset'),
    );
    // Not done:
    // $blocks['git_data'] = array(
    //               'info' => t('Latest commit messages'),
    //             );
    $blocks['amazon'] = array(
      'info' => t('Amazon Books'),
    );
    $blocks['amplify_tagcloud'] = array(
      'info' => t('Amplify Tag Cloud'),
    );
    $blocks['amplify_amazon'] = array(
      'info' => t('Amplify Amazon'),
    );
    $blocks['amplify_twitter'] = array(
      'info' => t('Amplify Twitter'),
    );
    $blocks['amplify_basics'] = array(
      'info' => t('Amplify Basics'),
    );
    $blocks['amplify_nouns'] = array(
      'info' => t('Amplify Proper Nouns'),
    );
    $blocks['amplify_sparql'] = array(
      'info' => t('Amplify SPARQL'),
    );
    $blocks['amplify_flickr'] = array(
      'info' => t('Amplify Flickr'),
    );
    $blocks['amplify_technorati'] = array(
      'info' => t('Amplify Technorati'),
    );
    $blocks['technorati'] = array(
      'info' => t('Technorati'),
    );
    $blocks['amplify_bloglines'] = array(
      'info' => t('Amplify Bloglines'),
    );
    $blocks['amplify_shoppingcom'] = array(
      'info' => t('Amplify Shopping.Com'),
    );
    return $blocks;
  }
  else {
    $function = '_' . __FUNCTION__ . '_' . $delta;
    // Ignore unknown deltas, as core mods do.
    if (function_exists($function)) {
      try {
        return $function($op, $delta, $edit);
      }
      catch (Exception $e) {
        drupal_set_message('An error occured retrieving some content', 'error');
        watchdog('qpservices', 'Error retrieving block: @err', array('@err' => $e->getMessage()), WATCHDOG_ERROR,  l('Name','path'));
        //drupal_set_message(check_plain($e->getMessage()), 'error');
        //drupal_set_message('Stack: ' . check_plain($e->getTraceAsString()), 'status');
      }
    }
  }
}

/**
 * Block helper function for qpservices.
 */
function _qpservices_block_twitter($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Twitter');
      $block['content'] = qpservices_twitter_search();
      return $block; 
  }
}
function _qpservices_block_amazon($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Books');
      $block['content'] = qpservices_amazon_books();
      return $block; 
  }
}
function _qpservices_block_flickr($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Flickr');
      $block['content'] = qpservices_flickr();
      return $block; 
  }
}
function _qpservices_block_flickrset($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Flickr set');
      $block['content'] = qpservices_flickrset();
      return $block; 
  }
}
function _qpservices_block_amplify_flickr($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Flickr');
      $block['content'] = qpservices_amplify_flickr();
      return $block; 
  }
}
function _qpservices_block_amplify_technorati($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Technorati Blogs');
      $block['content'] = qpservices_amplify_technorati();
      return $block; 
  }
}
function _qpservices_block_amplify_bloglines($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Bloglines Blogs');
      $block['content'] = qpservices_amplify_bloglines();
      return $block; 
  }
}

function _qpservices_block_technorati($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Technorati Blogs');
      $block['content'] = qpservices_technorati();
      return $block; 
  }
}
function _qpservices_block_git_data($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('GIT');
      $block['content'] = qpservices_git_data();
      return $block; 
  }
}
function _qpservices_block_amplify_tagcloud($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Ranking');
      $block['content'] = qpservices_amplify_tagcloud();
      return $block; 
  }
}
function _qpservices_block_amplify_twitter($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Twittersphere');
      $block['content'] = qpservices_amplify_twitter();
      return $block; 
    case 'configure':
      $form['qpservices_twitter_term_count'] = array(
        '#type' => 'select',
        '#title' => t('Twitter search term count'),
        '#description' => t('Number of identified terms that will be sent to twtiter to find related tweets.'),
        '#options' => array(
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
        ),
        '#default_value' => variable_get('qpservices_twitter_term_count', 1),
        '#required' => FALSE,
      );
      $form['qpservices_twitter_operator'] = array(
        '#type' => 'select',
        '#title' => t('Joining Operator'),
        '#description' => t('Operator to join the search terms'),
        '#options' => array(
          'OR' => t('Or (any of the terms)'),
          'AND' => t('And (all of the terms)'),
        ),
        '#default_value' => variable_get('qpservices_twitter_operator', 'OR'),
        '#required' => FALSE,
      );
      
      return $form;
    case 'save':
      variable_set('qpservices_twitter_term_count', (int)$edit['qpservices_twitter_term_count']);
      variable_set('qpservices_twitter_operator', $edit['qpservices_twitter_operator']);
  }
}
function _qpservices_block_amplify_basics($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('About This Page');
      $block['content'] = qpservices_amplify_basic();
      return $block; 
  }
}
function _qpservices_block_amplify_nouns($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Topics');
      $block['content'] = qpservices_amplify_propernouns();
      return $block; 
  }
}
  
function _qpservices_block_amplify_amazon($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Books');
      $block['content'] = qpservices_amplify_amazon();
      return $block; 
  }
}

function _qpservices_block_amplify_shoppingcom($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Shop');
      $block['content'] = qpservices_amplify_shoppingcom();
      return $block; 
  }
}


function _qpservices_block_amplify_sparql($op, $delta, $edit) {
  switch ($op) {
    case 'view':
      $block['subject'] = t('Semantic Web');
      $block['content'] = qpservices_amplify_sparql();
      return $block; 
  }
}

function qpservices_caching_qp($key, $ttl = '+2 weeks') {
  $data = QPCache::get($key);
  if (empty($data)) {
    $qp = qp($key);
    QPCache::set($key, $qp->xml(), strtotime($ttl));
  }
  else {
    try {
      $xml = $data->xml;
      $dom = new DOMDocument();
      $dom->loadXML($xml);
      
      // TODO: Find out why QP will not load a DB file directly.
      $qp = qp($dom);
    }
    catch (Exception $e) {
      drupal_set_message('Error: ' . check_plain($e->getMessage()), 'status');
    }
  }
  return $qp;
}

/**
 * Search Twitter for tweets about QueryPath.
 *
 * Search info comes back in an Atom feed.
 */
function qpservices_twitter_search($query = 'QueryPath') {
  $url = 'http://search.twitter.com/search.atom?rpp=3&q=' . urlencode($query);
  
  $qp = qpcache_qp($url, strtotime('+ 30 minutes'));
  $out = qp('<?xml version="1.0"?><div/>');
  foreach ($qp->find('entry') as $result) {
    $content = $result->children('content')->text();
    $content = filter_xss($content, array('a', 'b', 'em', 'i', 'strong'));
    $img = $result->siblings('link[rel="image"]')->attr('href');
    $author = $result->parent()->find('author>name')->text();
    $out->append('<div><img src="'. $img .'" width="48" height="48"/><br/><strong class="twitter-name">' . check_plain($author) . 
      '</strong><br/>' . $content . '</div>');
  }
  return $out->xml();
}

/**
 * Show recent photos.
 */
function qpservices_flickr() {
  $url = 'http://api.flickr.com/services/rest/?';

  $args = array(
    'api_key' => variable_get('qpservices_flickr_key', ''),
    //'extras' => 'tags',
    'per_page' => 10,
    'method' => 'flickr.interestingness.getList',
  );
  $url .= http_build_query($args, NULL, '&');
  
  $out = qp("<?xml version='1.0'?><div class='flickr'/>");
  $qp = qpcache_qp($url, '+2 days');
  foreach ($qp->find('photo') as $photo) {
    //print $photo->attr('title') . PHP_EOL;

    $farm = $photo->attr('farm');
    $server = $photo->attr('server');
    $id = $photo->attr('id');
    $secret = $photo->attr('secret');
    $user_id = $photo->attr('owner');
    $img = flickurl($farm, $server, $id, $secret, 's');
    $link = flickrlink($user_id, $id);
    $out->append('<a href="'. $link . '"><img src="' . $img . '"/></a>');

  }

  return $out->top()->find('.flickr')->html();
}

function qpservices_flickrset($set = '72157594352657197') {
  $url = 'http://api.flickr.com/services/rest/?';

  $args = array(
    'api_key' => variable_get('qpservices_flickr_key', ''),
    //'extras' => 'tags',
    'per_page' => 10,
    'method' => 'flickr.photosets.getPhotos',
    'photoset_id' => $set,
  );
  $url .= http_build_query($args, NULL, '&');
  
  $out = qp("<?xml version='1.0'?><div class='flickr'/>");
  $qp = qpcache_qp($url, '+1 week');
  
  foreach ($qp->find('photo') as $photo) {
    //print $photo->attr('title') . PHP_EOL;

    $farm = $photo->attr('farm');
    $server = $photo->attr('server');
    $id = $photo->attr('id');
    $secret = $photo->attr('secret');
    $user_id = $photo->parent()->attr('owner');
    $img = flickurl($farm, $server, $id, $secret, 's');
    $link = flickrlink($user_id, $id);
    $out->append('<a href="'. $link . '"><img src="' . $img . '"/></a>');

  }

  return $out->top()->find('.flickr')->html();
}

function qpservices_flickrtags($tags = array('milkshake')) {
  //return print_r($tags, TRUE);
  
  $url = 'http://api.flickr.com/services/rest/?';
  
  $args = array(
    'api_key' => variable_get('qpservices_flickr_key', ''),
    //'extras' => 'tags',
    'method' => 'flickr.tags.getClusterPhotos',
    'tag' => $tags[0],
    //'cluster_id' => 'cherry-diner-whippedcream',
  );
  $url .= http_build_query($args, NULL, '&');
  
  $out = qp("<?xml version='1.0'?><div class='flickr'/>");
  $qp = qp($url);//qpcache_qp($url, '+1 week');
  
  foreach ($qp->find('photo')->slice(0, 10) as $photo) {
    //print $photo->attr('title') . PHP_EOL;

    $farm = $photo->attr('farm');
    $server = $photo->attr('server');
    $id = $photo->attr('id');
    $secret = $photo->attr('secret');
    $user_id = $photo->attr('owner');
    $img = flickurl($farm, $server, $id, $secret, 's');
    $link = flickrlink($user_id, $id);
    $out->append('<a href="'. $link . '"><img src="' . $img . '"/></a>');

  }

  return $out->top()->find('.flickr')->html();
}

function qpservices_amplify_flickr() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    $tags = array();
    foreach ($qp->find('TopTopics>TopicResult')->slice(0, 10) as $topic) {
      $name = $topic->find('Topic>Name')->text();
      $tags[] = strtolower(str_replace(' ', '', $name));
    }
    
    return qpservices_flickrtags($tags);
  }
}

/**
 * @deprecated The API has been removed from Technorati.
 */
function qpservices_amplify_technorati() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    $tags = array();
    foreach ($qp->find('TopTopics>TopicResult')->slice(0, 3) as $topic) {
      $name = $topic->find('Topic>Name')->text();
      $tags[] = $name;//strtolower(str_replace(' ', '', $name));
    }
    
    return qpservices_technorati(implode(' ', $tags));
  } 
}

function qpservices_amplify_bloglines() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    $tags = array();
    foreach ($qp->find('TopTopics>TopicResult')->slice(0, 3) as $topic) {
      $name = $topic->find('Topic>Name')->text();
      $tags[] = $name;//strtolower(str_replace(' ', '', $name));
    }
    
    return qpservices_bloglines(implode(' ', $tags));
  }
}

function qpservices_bloglines($query='Drupal') {
  $url = 'http://www.bloglines.com/search?';
  $params = array(
    'q' => $query,
    'ql' => 'en',
    's' => 'f',
    'pop' => 'l',
    'news' => 'm',
    'format' => 'rss',
  );
  $url .= http_build_query($params, NULL, '&');
  
  $qp = qpcache_qp($url);
  
  $out = '<div class="blog">';
  foreach ($qp->find('item') as $item) {
    $title = $item->children('title')->text();
    $link = $item->siblings('link')->text();
    $excerpt = filter_xss_admin($item->siblings('description')->text());
    $parts = parse_url($link);
    //$blogname = $item->parent()->find('weblog>name')->text();
    //$blogurl = $item->siblings('url')->text();
    $out .= '<h4><a href="'. $link .'">' . $title . '</a></h4><p>' . $excerpt
    . ' [<a href="' . $parts['schema'] . '://' . $parts['host'] . '">' . $parts['host']
    . '</a>]</p>';
  }
  $out .= '</div>';
  
  return $out;
}

/**
 * @deprecated The API has been removed from Technorati.
 */
function qpservices_technorati($query='Drupal') {
  $id = variable_get('qpservices_technorati_key', '');
  $url = 'http://api.technorati.com/search?';
  
  $params = array(
    'key' => $id,
    'query' => $query,
    'limit' => 5,
    'authority' => 'a4',
    'language' => 'en',
  );
  
  $url .= http_build_query($params, NULL, '&');
  
  //$f = file_get_contents($url);
  //drupal_set_message('<pre>' . htmlentities(print_r($f)) . '</pre>');
  //drupal_set_message(l($url, $url, array('absolute' => TRUE)), 'status');
  
  //return qp($url)->xml(TRUE);
  $qp = qpcache_qp($url);
  
  $out = '<div class="blog">';
  foreach ($qp->find('item') as $item) {
    $title = $item->children('title')->text();
    $link = $item->siblings('permalink')->text();
    $excerpt = filter_xss_admin($item->siblings('excerpt')->text());
    $blogname = $item->parent()->find('weblog>name')->text();
    $blogurl = $item->siblings('url')->text();
    $out .= '<h4><a href="'. $link .'">' . $title . '</a></h4><p>' . $excerpt . 
      '... [<a href="'. $blogurl .'">'. $blogname . 
      '</a>]</p>';
  }
  $out .= '</div>';
  
  return $out;
}

function qpservices_amplify_shoppingcom() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    $qp->find('ProperNouns>TopicResult>Topic>Name')->slice(0, 2);

    if ($qp->size() > 0) {
      $keywords = '';
      foreach ($qp as $keyword) {
        $keywords .= $keyword->text() . ' ';
      }
      $out = qpservices_shoppingcom($keywords);
      return $out;
    }
  }
}

function qpservices_shoppingcom($keyword) {
  $format = '<p><a href="!url"><img src="!img"/><br/>@title</a><br/>$@price</p>';
  $url = 'http://publisher.usb.api.shopping.com/publisher/3.0/rest/GeneralSearch?';
  $id = variable_get('qpservices_shoppingcom_key', '');
  $tracking = variable_get('qpservices_shoppingcom_tracking', '');
  $params = array(
    'apiKey' => '78b0db8a-0ee1-4939-a2f9-d3cd95ec0fcc',
    'trackingId' => '7000610',
    'keyword' => $keyword,
    'numItems' => 10,
  );
  
  $url .= http_build_query($params, NULL, '&');
  $qp = qpcache_qp($url);
  
  foreach ($qp->find('product') as $item) {
    $values['@title'] = $item->branch('name')->text();
    $values['!img'] = $item->branch('images>image:first>sourceURL')->text();
    $values['!url'] = $item->branch('productOffersURL')->text();
    $values['@price'] = $item->branch('minPrice')->text() . '-' . $item->branch('maxPrice')->text();
    
    $out .= strtr($format, $values);
  }
  return $out;
}

/**
 * Get info from the Git RSS feed and clean it.
 */
function qpservices_git_data() {
  $url = '';
}

/**
 * Access Amazon and search for books.
 * @param $query
 *  An 'Power' query against Amazon.
 */
function qpservices_amazon_books($query = 'author-exact:Matt Butcher') {
  $id = variable_get('qpservices_amazon_key', '');
  $format = '<p><a href="!url"><img src="!img"/><br/>@title</a><br/>@author</p>';
  $url = 'http://ecs.amazonaws.com/onca/xml?';
  
  $params = array(
    'Service' => 'AWSECommerceService',
    'AWSAccessKeyId' => $id,
    'Operation' => 'ItemSearch',
    'SearchIndex' => 'Books',
    'ResponseGroup' => 'Medium',
    'Sort' => 'daterank',
    'Power' => $query,
  );
  
  $url .= http_build_query($params, NULL, '&');
  
  $qp = qpcache_qp($url);
  $out = ''; // . $query;
  
  foreach ($qp->find('Item') as $item) {
    $values['!url'] = $item->children('DetailPageURL')->text();
    $values['!img'] = $item->parent()->find('SmallImage>URL')->text();
    //$values['!content'] = $item->end()->find('EditorialReview Content')->text();
    $values['@title'] = $item->end()->children('ItemAttributes>Title')->text();
    $authors = array();
    foreach ($item->end()->find('ItemAttributes>Author') as $author) {
      $authors[] = $author->text();
    }
    $values['@author'] = implode(', ', $authors);
    $values['@title'] = $item->siblings('Title')->text();
    
    

    $out .= strtr($format, $values);
  }
  
  return $out;
}

function qpservices_amplify_tags($qp, $node) {
  drupal_add_css(drupal_get_path('module', 'qpservices') . '/qpservices.css');
  $out = qp('<?xml version="1.0"?><div class="tagcloud"/>', 'div');
  foreach ($qp->find('TopTopics>TopicResult')->slice(0, 10) as $topic) {
    $name = $topic->find('Topic>Name')->text();
    $polarity = $topic->end()->find('Polarity>Mean>Name')->text();
    $guidance = $topic->end()->find('OfferingGuidance>Name')->text();
    
    $pol_class = 'polarity-' . strtolower($polarity);
    $guid_class = 'guidance-' . strtr(strtolower($guidance), ' ', '-');
    $classes = $pol_class . ' ' . $guid_class . ' amplified-tags';
    
    $out->append('<span class="' . $classes . '">' . htmlentities($name) . '</span> ');
  }
  return $out;
}

function qpservices_amplify_tagcloud() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    $out = qpservices_amplify_tags($qp, $node);
    return $out->find('div')->html();
  }
}

function qpservices_amplify_basic() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    // Since we are doing massive queries, we do fastest 
    // search possible: Lots of direct children.
    $v = array();
    $v['Domain'] = $qp->find('Domains>DomainResult>Domain>Name')->text();
    $v['Subdomain'] = $qp->top()->find('Domains>DomainResult>Subdomains>SubdomainResult>Subdomain>Name')->text();
    
    $dtopics = $qp->top()->find('Domains>DomainResult>Subdomains>SubdomainResult>Scores>TopicResult>Topic>Name');
    $topics = array();
    foreach ($dtopics as $topic) {
      $topics[] = '<a href="#">' . $topic->text() . '</a>';
    }
    $v['Topics'] = implode(', ', $topics);
    
    $locations = array();
    foreach ($qp->top()->find('Locations>Result>Name') as $loc) {
      $locations[] = $loc->text();
    }
    $v['Locations'] = implode(', ', $locations);
    
    
    $ed = $qp->top()->find('Demographics>Education>Name')->text();
    $slang = $qp->top()->find('Styles>Slang>Name')->text();
    $flamboyance = $qp->top()->find('Styles>Flamboyance>Name')->text();
    
    
    $out = qp('<?xml version="1.0" ?><ul/>');
    foreach ($v as $label => $value) {
      $t = '<li><strong>' . $label . '</strong>: ' . $value . '</li>';
      $out->append($t);
    }
    
    $base = base_path() . 'svg/amplify/' . $node->nid . '/'; 
    $imgs = array(
      $base . 'flamboyance.svg' => array('Flamboyance', $flamboyance),
      $base . 'slang.svg' => array('Slang', $slang),
      $base . 'education.svg' => array('Education', $ed),
    );
    foreach ($imgs as $img => $title) {
      $out->append('<li><strong>'. $title[0] .':</strong> ' . $title[1]. '<br/>
        <iframe src="'.$img.'" height="70" width="160" type="image/svg+xml" style="border:0px">
        </iframe></li>');
    }
    
    //$out->append(qpservices_amplify_slang_svg(1.0));
    /*
    $out = qp();
    $out->append(
      '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid slice" style="width: 100%; height: 100%; position: absolute; top: 0pt; left: 0pt; z-index: -1;">
          <linearGradient id="gradient">
            <stop class="begin" offset="0%"/>
            <stop class="end" offset="100%"/>
          </linearGradient>
          <rect x="0" y="0" width="100" height="100" style="fill: url(#gradient) rgb(0, 0, 0);"/>

          <circle cx="50" cy="50" r="30" style="fill: url(#gradient) rgb(0, 0, 0);"/>
        </svg>
      '
    );
    */

    
    
    return $out->top()->html();
  }
}

function qpservices_amplify_propernouns() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    $qp->find('ProperNouns>TopicResult>Topic>Name')->slice(0, 20);
    
    $out = qp('<?xml version="1.0"?><ul/>');
    $temp = '<li><a href="%s">%s</a></li>';
    foreach ($qp as $name) {
      $out->append(sprintf($temp, '#', $name->text()));
    }
    return $out->top()->find('ul')->xml();
  }
}

function qpservices_sparql($topic) {
  // URL to DB Pedia's SPARQL endpoint.
  $url = 'http://dbpedia.org/sparql';

  // The SPARQL query to run.
  $sparql = '
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?uri ?name ?label
    WHERE {
      ?uri foaf:name ?name .
      ?uri rdfs:label ?label
      FILTER (?name = "The Beatles")
      FILTER (lang(?label) = "en")
    }
  ';

  // We first set up the parameters that will be sent.
  $params = array(
    'query' => $sparql,
    'format' => 'application/sparql-results+xml',
  );
  // DB Pedia wants a GET query, so we create one.
  $data = http_build_query($params);
  $url .= '?' . $data;
  
  $qp = qpcache_qp($url);
  return $qp;
}

function qpservices_amplify_sparql() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    $qp->find('ProperNouns>TopicResult:first>Topic>Name');
    
    if ($qp->size() > 0) {
      
      
      //return $out->find('div')->xml();
      return $out;
    }
  }
}

function qpservices_amplify_amazon() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    $qp->find('ProperNouns>TopicResult>Topic>Name')->slice(0, 2);
    //$qp->find('Topics>Domains>DomainResult>Subdomains>SubdomainResult>Scores>TopicResult:first>Topic>Name')->slice(0, 2);
    //drupal_set_message('Search: ' . $qp->text(), 'status');
    
    if ($qp->size() > 0) {
      $keywords = '';
      foreach ($qp as $keyword) {
        $keywords .= $keyword->text() . ' ';
      }
      $out = qpservices_amazon_books('keywords:' . $keywords);
      //return $out->find('div')->xml();
      return $out;
    }
  }
}

function qpservices_amplify_twitter() {
  if (arg(0) == 'node' && ctype_digit(arg(1))) {
    $node = node_load(arg(1));
    $qp = amplify_get_qp($node);
    if (empty($qp)) {
      return '';
    }
    
    //$search = $qp->find('TopTopics>TopicResult:first>Topic>Name')->text();
    $sep = sprintf(' %s ', variable_get('qpservices_twitter_operator', 'OR'));
    $search = $qp->find('TopTopics>TopicResult>Topic>Name')->slice(0,variable_get('qpservices_twitter_term_count', 1))->textImplode($sep);
    $out = '';
    //$out = '<em>What they\'re saying about <strong>' . $search . '</strong></em>';
    $out .= qpservices_twitter_search($search);
    return $out;//$out->find('div')->xml();
  }
}

function qpservices_amplify_flamboyance_svg($nid) {
  $node = node_load($nid);
  $qp = amplify_get_qp($node);
  $rating = $qp->top()->find('Styles>Flamboyance>Value')->text();
  qpservices_amplify_svg($rating, 30);
}

function qpservices_amplify_education_svg($nid) {
  $node = node_load($nid);
  $qp = amplify_get_qp($node);
  $rating = $qp->top()->find('Demographics>Education>Value')->text();
  // 150 / 5 = 30
  qpservices_amplify_svg($rating, 30);
}

function qpservices_amplify_slang_svg($nid) {
  $node = node_load($nid);
  $qp = amplify_get_qp($node);
  $rating = $qp->top()->find('Styles>Slang>Value')->text();
  // 150 / 100 = 1.5
  return qpservices_amplify_svg($rating, 1.5);
}

/**
 * Build an SVG based on Amplify.
 */
function qpservices_amplify_svg($rating, $multiplier = 50, $colors = array('white', 'purple')) {
  // Build base SVG.
  $svg_stub = '<?xml version="1.0"?>
  <svg:svg
     xmlns:svg="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     version="1.1"
     width="150"
     height="60"
     viewbox="0 0 150 60"
     style="position: relative"
     baseProfile="full"
     id="test">
    <svg:desc>Created by QueryPath.</svg:desc>
    <svg:linearGradient id="stdGradient">
      <svg:stop offset="5%" stop-color="#F60" />
      <svg:stop offset="95%" stop-color="#FF6" />
    </svg:linearGradient>
    <svg:rect id="first" x="0" y="4" width="150" height="48" fill="url(#stdGradient)"
      stroke="black" stroke-width="1"/>
    <svg:rect id="line" y="0" width="5" height="60" fill="black"/>
  </svg:svg>';
  // 150 / 5 = 30
  $coord_x = floor($rating * $multiplier);
  header('Content-Type: image/svg+xml');
  qp($svg_stub, '#line')->attr('x', $coord_x)
    ->top()->find('#stdGradient>:first')->attr('stop-color', $colors[0])
    ->next()->attr('stop-color', $colors[1])
    ->writeXML();
  exit;
}

function flickurl($farm, $server, $id, $secret, $size = 'm') {
  $format = 'http://farm%s.static.flickr.com/%s/%s_%s_%s.jpg';
  return sprintf($format, $farm, $server, $id, $secret, $size);
}
function flickrlink($user_id, $id) {
  $format = 'http://flickr.com/photos/%s/%s';
  return sprintf($format, $user_id, $id);
}

// ============================================================================


function qpservices_amplify_key($nid, $modifier = '') {
  // Generate a *correct* URI. No unnecessary slashes.
  $base = 'amplify:node/' . $nid;
  if (strlen($modifier) > 0) {
    $base .= '/' . $modifier;
  }
  return $base;
}