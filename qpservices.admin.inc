<?php

/**
 * qpservices.admin
 *
 * @file
 */

/**
 * Administrative form for QP services.
 */
function qpservices_admin_form() {
  
  $form['qpservices_amazon_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Amazon API Key'),
    '#description' => t('Enter your Amazon API key here to use the Amazon features.'),
    '#default_value' => variable_get('qpservices_amazon_key', ''),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  $form['qpservices_flickr_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr API Key'),
    '#description' => t('Enter your developer API key for Flickr.'),
    '#default_value' => variable_get('qpservices_flickr_key', ''),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  $form['qpservices_technorati_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Technorati API Key'),
    '#description' => t('Enter your developer API key for the Technorati web service'),
    '#default_value' => variable_get('qpservices_technorati_key', ''),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  $form['qpservices_shoppingcom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shopping.Com'),
    '#description' => t('Shopping.Com uses both an API key and a tracking code.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['qpservices_shoppingcom']['qpservices_shoppingcom_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopping.Com API Key'),
    '#description' => t('Enter your developer API key for Shopping.Com'),
    // The sample API key.
    '#default_value' => '78b0db8a-0ee1-4939-a2f9-d3cd95ec0fcc',
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  $form['qpservices_shoppingcom']['qpservices_shoppingcom_tracking'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopping.Com Tracking Code'),
    '#description' => t('Enter your tracking code. This is assigned along with the API key.'),
    // The sample tracking code.
    '#default_value' => '7000610',
    '#size' => 8,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  /*
  $form['qpservices_amplify_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Amplify API Key'),
    '#description' => t('Enter your developer API key for the OpenAmplify web service'),
    '#default_value' => variable_get('qpservices_amplify_key', ''),
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => FALSE,
  );
  
  
  // Get node types that OpenAmplify should submit.
  $types = node_get_types();
  $type_list = array();
  foreach ($types as $key => $type) {
    $type_list[$key] = $type->name;
  }
  $form['qpservices_amplify_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Amplified Node Types'),
    '#description' => t('List of node types that will be amplified by OpenAmplify.'),
    '#options' => $type_list,
    '#default_value' => variable_get('qpservices_amplify_node_types', array()),
    '#required' => FALSE,
  );
  */
  
  return system_settings_form($form);
}

